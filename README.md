# Configuration

Modify the **CONFIG** section and just run the python script.

# Systemd

In order to run the script automatically at boot, the following SystemD unit file can be used:

```
[Unit]
Description=MeteoFrance to MQTT
Wants=network.target
After=network.target

[Service]
Type=simple
User=meteofrance
ExecStart=/usr/bin/python /home/meteofrance/weboob-meteofrance-mqtt/meteofrance.py
Restart=Always

[Install]
WantedBy=multi-user.target
```
Of course this has to be adapted to your users and paths.