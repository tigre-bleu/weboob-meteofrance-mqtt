#!/usr/bin/python
# -*- coding: utf-8 -*-

# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with weboob. If not, see <http://www.gnu.org/licenses/>.

from weboob.core import Weboob
from weboob.capabilities.weather import CapWeather, City
import paho.mqtt.client as mqtt
import time
import re
import argparse

class App(object):

    def mqtt_on_connect(self, mqttc, userdata, flags, rc):
        print("MQTT Client Connected")
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        (result, mid)=mqttc.subscribe("weather/commands/#")
        (result, mid)=mqttc.subscribe("weather/client/#")


    def mqtt_on_message(self, mqttc, userdata, msg):
        print(msg.topic)
        print(msg.payload)
        if msg.topic == "weather/client":
            if msg.payload == "ONLINE":
                print("Client just went online")
                self.send_to_mqtt()
        elif msg.topic == "weather/commands":
            if msg.payload == "REFRESH":
                print("Weather box requested a refresh")
                self.update()
                self.send_to_mqtt()

    def __init__(self, broker, topic, city_ids):
        self.w = Weboob()
        self.w.load_backends(CapWeather)
        self.cities = []
        self.current={}
        self.forecast={}
        for city_id in city_ids:
            self.cities+=list(self.w.iter_city_search(str(city_id)))

        for city in self.cities:
            self.current[city.id] = None
            self.forecast[city.id] = None

        self.mqtt_topic_root = topic

        self.mqttc = mqtt.Client()
        self.mqttc.on_connect = self.mqtt_on_connect
        self.mqttc.on_message = self.mqtt_on_message

        self.mqttc.connect(broker, 1883, 60)

        self.mqttc.loop_start()

        self.sleep_time = 0.4

    def update(self):
        for city in self.cities:
            self.current[city.id] = list(self.w.get_current(city.id))
            self.forecast[city.id] = list(self.w.iter_forecast(city.id))

    def send_to_mqtt(self):
        regex_current_text = re.compile(r"^Ressenti (?P<felt>-*\d+).C - (?P<text>.*) - UV \d+ - Vent [< ]*(?P<wind>\d+) km\/h")

        for city in self.cities:
            current = self.current[city.id][0]
            if current is not None:
                self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/current/date", current.date.isoformat())
                time.sleep(self.sleep_time)
                self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/current/temperature", str(current.temp.value))
                time.sleep(self.sleep_time)

                m = regex_current_text.match(current.text)

                if m:
                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/current/temperature_felt", m.group('felt'))
                    time.sleep(self.sleep_time)

                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/current/text", m.group('text'))
                    time.sleep(self.sleep_time)

                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/current/wind", m.group('wind'))

            forecasts = self.forecast[city.id]
            if forecasts is not None:
                today = True
                for forecast in list(forecasts)[0:2]:
                    if today:
                        topic="today"
                        today = False
                    else:
                        topic="tomorrow"
                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/"+topic+"/date", forecast.date.isoformat())
                    time.sleep(self.sleep_time)
                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/"+topic+"/text", forecast.text)
                    time.sleep(self.sleep_time)
                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/"+topic+"/temperature_low", forecast.low.value)
                    time.sleep(self.sleep_time)
                    self.mqttc.publish(self.mqtt_topic_root+"/"+city.name+"/"+topic+"/temperature_high", forecast.high.value)
                    time.sleep(self.sleep_time)


if __name__=="__main__":

    # CONFIGURATION
    POSTCODE = 31000
    REFRESH_PERIOD = 1800 #Seconds
    BROKER = "localhost"
    MQTT_TOPIC = "weather"

    app=App(BROKER, MQTT_TOPIC, [POSTCODE])

    while(True):
        app.update()
        app.send_to_mqtt()

        time.sleep(REFRESH_PERIOD)
